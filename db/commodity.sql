﻿-- Table: commodity

-- DROP TABLE commodity;

CREATE TABLE commodity
(
  id bigserial NOT NULL,
  name text NOT NULL,
  season_price numeric NOT NULL,
  non_season_price numeric NOT NULL,
  measure text,
  description text,
  CONSTRAINT commodity_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE commodity
  OWNER TO postgres;
