﻿CREATE TABLE request
(
  id bigserial NOT NULL PRIMARY KEY,
  commodity_id integer NOT NULL REFERENCES commodity (id),
  quantity integer NOT NULL,
  name text NOT NULL,
  description text

)
WITH (
  OIDS=FALSE
);
ALTER TABLE request
  OWNER TO GOIT;
