DROP TABLE commodity CASCADE;
DROP TABLE request CASCADE;
DROP TABLE response CASCADE;
DROP SEQUENCE commodity_id_seq;
DROP SEQUENCE request_id_seq;
DROP SEQUENCE response_id_seq;

CREATE SEQUENCE commodity_seq;
CREATE SEQUENCE request_seq;
CREATE SEQUENCE response_seq; 

CREATE TABLE commodity
(
  id INT NOT NULL DEFAULT NEXTVAL('commodity_seq'),
  name VARCHAR(128) NOT NULL,
  season_price DECIMAL(10, 4) NOT NULL,
  non_season_price DECIMAL(10, 4) NOT NULL,
  measure VARCHAR(5),
  description VARCHAR(1024),
  CONSTRAINT commodity_pkey PRIMARY KEY (id)
);

ALTER SEQUENCE commodity_seq OWNED BY commodity.id;

CREATE TABLE request
(
  id INT NOT NULL DEFAULT NEXTVAL('request_seq'),
  commodity_id INT NOT NULL REFERENCES commodity (id),
  quantity INT NOT NULL,
  name VARCHAR(128) NOT NULL,
  description VARCHAR(1024),
  CONSTRAINT request_pkey PRIMARY KEY (id)
);

ALTER SEQUENCE request_seq OWNED BY request.id;

CREATE TABLE response
(
  id INT NOT NULL DEFAULT nextval('response_seq'::regclass),
  name VARCHAR(128) NOT NULL,
  status VARCHAR NOT NULL,
  request INT NOT NULL,
  description VARCHAR(1024),
  CONSTRAINT response_pkey PRIMARY KEY (id),
  CONSTRAINT response_request_id_fkey FOREIGN KEY (request)
      REFERENCES request (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER SEQUENCE response_seq OWNED BY response.id;
<<<<<<< HEAD
ALTER TABLE response
  OWNER TO goit;
=======

INSERT INTO commodity (name, season_price, non_season_price, measure, description) VALUES
('Milk', 12, 22, 'liter', 'Very delicious milk'),
('Bread', 2, 3, 'piece', 'Fresh and healthy'),
('Sand', 1, 1, 'ton', 'Great heap of sand. Pikup only'),
('Rock', 1, 1, 'kg', 'A usual rock. Can be shattered'),
('Coat', 125, 125, 'piece', 'Warm, black and shiny'),
('Hat', 70, 70, 'piece', 'It lookis like Frank Sinatra`s hat'),
('Wheat', 500, 800, 'ton', 'Grainy wheat'),
('Table', 230, 220, 'piece', 'Wooden table, best for garden and home'),
('Chair', 115, 115, 'piece', 'You can sit on it'),
('Dance', 120, 120, 'piece', 'I can dance for you. Really');
>>>>>>> branch 'develop' of https://Alex_Sash@bitbucket.org/lslayer/solidcom.git
