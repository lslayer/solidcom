<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>Getting a List of Commodities</title>
</head>
<body>
	
	<s:actionerror cssClass="errorTable" />
    
    <s:form name="paginatorPanel" theme="simple" action= "CommodityList" >
		<jsp:include page="/AppPages/Paginator.jsp" />
	</s:form>
		
    <table border="1" style="width: 60%">
		<tr>
			<td>id
			<td>Name
			<td>Price
			<td>Non season price
		<tr>
			<s:iterator value="commodities" status="stat">
				<tr>
					<s:form name="CommodityList" theme="simple" action="CommodityList">
						<td><s:property value="commodities[#stat.index].id" /></td>
						<td><s:property value="commodities[#stat.index].name" /></td>
						<td><s:property value="commodities[#stat.index].price" /></td>
						<td><s:property
								value="commodities[#stat.index].nonSeasonPrice" /></td>
					</s:form>
					<s:form name="CommodityDetailer" theme="simple"
						action="CommodityDetailer">
						<td><button name="id" type="submit"
								value=<s:property value = "commodities[#stat.index].id"/>>
								details</button></td>
					</s:form>
					<s:form name="CommodityDeleter" theme="simple"
						action="CommodityDeleter">
						<td><button name="id" type="submit"
								value=<s:property value = "commodities[#stat.index].id"/>>
								delete</button></td>
					</s:form>
					<s:form name="CommodityRetriver" theme="simple"
						action="CommodityRetriver">
						<td><button name="id" type="submit"
								value=<s:property value = "commodities[#stat.index].id"/>>
								update</button></td>
					</s:form>
				<tr>
			</s:iterator>
	</table>

	<a href="index.jsp">вернуться на главное меню</a>
</body>
</html>

