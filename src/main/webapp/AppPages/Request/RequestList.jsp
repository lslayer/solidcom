<%@ page language="java" contentType="text/html; charset=UTF-8"
	 pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" rel="stylesheet" href="design.css">
		<title>Getting a List of Requests</title>
	</head>
<body>
	
    <s:actionerror cssClass="errorTable" />
    <s:form name="paginatorPanel" theme="simple" action= "RequestList" >
		<jsp:include page="/AppPages/Paginator.jsp" />
	</s:form>
	
        <table border="1" style="width: 45%">
			<tr>
				<td>id</td>
				<td>Name</td>
				<td>Commodity</td>
				<td>Quantity</td>
				<td>Description</td>
			<tr>
				<s:iterator  value="requestList" status="stat">
	            	<tr>
						<s:form name="RequestList" theme="simple" action="RequestList">
							<td><s:property value = "requestList[#stat.index].id"/></td>
							<td><s:property value = "requestList[#stat.index].name"/></td>
							<td><s:property value = "requestList[#stat.index].commodity.getName()"/></td>
							<td><s:property value = "requestList[#stat.index].quantity"/></td>
							<td><s:property value = "requestList[#stat.index].description"/></td>
						</s:form>
                        <s:form name="RequestDeleter" theme="simple" action="RequestDeleter">
                            <td><button name="id" type="submit"
                                    value= <s:property value = "requestList[#stat.index].id"/>
                                > delete</button>
                            </td>
                        </s:form>
                        <s:form name="RespondToRequest" theme="simple" action="RespondToRequest">
                            <td><button name="id" type="submit"
                                    value= <s:property value = "requestList[#stat.index].id"/>
                                > respond</button>
                            </td>
                        </s:form>
						<s:form name="RequestUpdater" theme="simple" action="RequestUpdater">
							<td><button name="id" type="submit" 
									value= <s:property value = "requestList[#stat.index].id"/>
								> update</button>
							</td>
						</s:form>
						<s:form name="RequestDetails" theme="simple" action="RequestDetails">
                            <td><button name="id" type="submit" 
                                    value= <s:property value = "requestList[#stat.index].id"/>
                                >details</button>
                            </td>
                        </s:form>		
					<tr>
				</s:iterator>		    	
		</table>

    <a href="index.jsp">вернуться на главное меню</a>
</body>
</html>

