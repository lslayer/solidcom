<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>Create request</title>
</head>
<body>
    
    <s:actionerror cssClass="errorTable" />
   
    <h2>Insert your request data</h2>
	<s:form action="RequestDetails">
	    <s:textfield name="name" label="request name" readonly="true"/>
	    <s:textfield name="commodityName" label="commodity" readonly="true"/>
        <s:textfield name="quantity" label="quantity" readonly="true"/>
        <s:textarea name="description" label="description" readonly="true"/>
        <s:form name="RespondToRequest" theme="simple"
         action="RespondToRequest">
             <td><button name="id" type="submit"
                         value=<s:property 
                         value = "id"/>>
                         respond
             </button></td>
        </s:form>
    <table border="1" style="width: 45%">
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Description</td>
            <td>Status</td>
            <td>Request</td>
        <tr>
            <s:iterator value="responses" status="stat">
                <tr>
                    <s:form name="ResponseList" theme="simple" action="ResponseList">
                        <td><s:property value="responses[#stat.index].id" /></td>
                        <td><s:property value="responses[#stat.index].name" /></td>
                        <td><s:property value="responses[#stat.index].description" /></td>
                        <td><s:property value="responses[#stat.index].status" /></td>
                        <td><s:property value="responses[#stat.index].request.id" /></td>
                    </s:form>
                    
                    <s:form name="ResponseAcceptor" theme="simple" action="ResponseAcceptor">
                        <td>
                        <input type="hidden" 
                               name="action" 
                               value="accept"/>
                               
                        <button name="id" 
                                type="submit"
                                value=<s:property value = "responses[#stat.index].id"/>>
                                accept</button>
                        </td>
                    </s:form>
                    <s:form name="ResponseAcceptor" theme="simple" action="ResponseAcceptor">
                        <td><input type="hidden" name="action" value="reject"/>
                        <button name="id" type="submit"
                                    value=<s:property value = "responses[#stat.index].id"/>>
                                    reject</button></td>
                    </s:form>
                <tr>
            </s:iterator>
    </table>

 </s:form>
	
    <a href="<s:url action='RequestList'/>">Вернуться к списку</a>
 </body>
</html>