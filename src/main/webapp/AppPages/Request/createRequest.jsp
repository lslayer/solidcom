<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>Create request</title>
</head>
<body>
    
    <s:actionerror cssClass="errorTable" />
   
    <h2>Insert your request data</h2>
	<s:form action="RequestCreate">
	    <s:hidden name="id" label="id"/>
	    <s:textfield name="name" label="request name"/>
	    <s:select list="commodityList"
	       label="Select your commodity" 
	       listValue="name"
	       listKey="id"
	       value="commodity" 
	       name="commodityId"/>
        <s:textfield name="quantity" label="quantity"/>
        <s:textarea name="description" label="description"/>
	    <s:submit/>
	</s:form>
    <a href="index.jsp">вернуться на главное меню</a>
 </body>
</html>