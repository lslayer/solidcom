<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>Editing</title>
</head>
<body>
	<s:actionerror cssClass="errorTable" />
	<table border="1" style="width: 45%">
		<tr>
			<td>id
			<td>Name
			<td>Description
			<td>Status
			<td>Request
		<tr>
			<s:form name="ResponseRetriever" theme="simple"
				action="ResponseRetriever">
				<tr>
					<td><s:property value="%{response.id}" />
					<td><s:property value="%{response.name}" />
					<td><s:property value="%{response.description}" />
					<td><s:property value="%{response.status}" />
					<td><s:property value="%{response.request.id}" />
				<tr>
			</s:form>
		<tr>
			<s:form name="ResponseUpdater" action="ResponseUpdater">
				<td><s:textfield name="id" label="id" />
				<td><s:textfield name="name" label="Name" />
				<td><s:textfield name="description" label="Description" />
				<td><s:textfield name="status" label="Status" />
				<td><s:textfield name="requestId" label="Request" value="%{response.request.id}" />
				<td><s:submit />
			</s:form>
	</table>
    <a href="index.jsp">back to main menu</a>
</body>
</html>
