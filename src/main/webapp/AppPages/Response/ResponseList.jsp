<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>Getting a list of response</title>
</head>
<body>
	<s:actionerror cssClass="errorTable" />
	<s:form name="paginatorPanel" theme="simple" action= "ResponseList" >
		<jsp:include page="/AppPages/Paginator.jsp" />
	</s:form>
	
	<table border="1" style="width: 45%">
		<tr>
			<td>Id</td>
			<td>Name</td>
			<td>Description</td>
			<td>Status</td>
			<td>Request</td>
		<tr>
			<s:iterator value="responses" status="stat">
				<tr>
					<s:form name="ResponseList" theme="simple" action="ResponseList">
						<td><s:property value="responses[#stat.index].id" /></td>
						<td><s:property value="responses[#stat.index].name" /></td>
						<td><s:property value="responses[#stat.index].description" /></td>
						<td><s:property value="responses[#stat.index].status" /></td>
						<td><s:property value="responses[#stat.index].request.id" /></td>
					</s:form>
					
					<s:form name="ResponseDeleter" theme="simple" action="ResponseDeleter">
						<td><button name="id" type="submit"
									value=<s:property value = "responses[#stat.index].id"/>>
									delete</button></td>
					</s:form>
					<s:form name="ResponseRetriever" theme="simple" action="ResponseRetriever">
						<td><button name="id" type="submit"
									value=<s:property value = "responses[#stat.index].id"/>>
									update</button></td>
					</s:form>
				<tr>
			</s:iterator>
	</table>

    <a href="index.jsp">back to main menu</a>
</body>
</html>
