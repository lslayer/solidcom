<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="design.css">
<title>Create response page</title>
</head>
<body>
	<s:actionerror cssClass="errorTable" />
	<h4>Please fill the forms</h4>
	<s:form name="ResponseCreater" action="ResponseCreater">
		<table border="0">
			<tr>
				<td><s:textfield name="name" label="Name" /></td>
			</tr>
			<tr>
				<td><s:textfield name="description" label="Description" /></td>
			</tr>
			<tr>
				<td><s:textfield name="requestId" label="Request" /></td>
				<td>
						    <s:select list="RequestList"
	       					label="Select your request" 
	       					listValue="name"
	       					listKey="id" 
	       					name="RequestId"/>
				</td>
			</tr>
			<tr>
                <td><s:textfield name="status" label="Status"/></td>
			</tr>
			<tr>
				<td><s:submit />
			</tr>
		</table>
	</s:form>
    <a href="index.jsp">back to main menu</a>
</body>
</html>
