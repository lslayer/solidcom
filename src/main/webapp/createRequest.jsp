<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create request</title>
</head>
<body>
	<s:form action="CreateRequest">
	    <s:textfield name="reauest name" label="name"/>
	    <s:textfield name="commodity id" label="commodity_id"/>
        <s:textfield name="quantity" label="quantity"/>
        <s:textfield name="description" label="description"/>
        <s:textfield name="test" label="testing online IDE"/>
	    <s:submit/>
	</s:form>
 
</body>
</html>