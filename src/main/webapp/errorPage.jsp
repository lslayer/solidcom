<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" href="design.css">
	<title>Solid.com System</title>
</head>
<body>

	<h1>Что-то пошло не так... </h1>
	<h2>Сервер сообщил об ошибке: ${param.message}</h2>
	
    <s:actionerror cssClass="errorTable" />
   
     
</body>
</html>