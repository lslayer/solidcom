package ua.com.goit.volonteurs.solidcommunity.web.actions.request;

import java.util.List;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.CommodityDao;
import ua.com.goit.volonteurs.solidcommunity.dao.RequestDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Commodity;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;

import com.opensymphony.xwork2.ActionSupport;

public class RequestAction extends ActionSupport{
	
	private static final long serialVersionUID = -4045234734003591555L;
	private static final Logger LOG = Logger.getLogger(RequestAction.class);
	
	private long id;
	private long commodityId;
	private int quantity;
	private String name;
	private String description;
	private List<Commodity> commodityList = null;
	
	public RequestAction(){
		
	}
	
	public String getCommodities(){
		
		CommodityDao commDao = ApplicationContextProvider.getApplicationContext().getBean(CommodityDao.class);
		try {
			commodityList = commDao.retrieveAll();
		} catch (SolidComDaoException e) {
			LOG.error("Can't find commodity:" + e.getMessage(), e);
			addActionError("Can't find commodity!");
		}
		
		return ActionSupport.SUCCESS;
	}
	
	public String execute(){
 
		Request request = new Request();
		Commodity commodity = new Commodity();
		RequestDao requestDao = ApplicationContextProvider.getApplicationContext().getBean(RequestDao.class);
		CommodityDao commDao = ApplicationContextProvider.getApplicationContext().getBean(CommodityDao.class);
					
		try {
			commodity = commDao.retrieve(commodityId);
		} catch (SolidComDaoException e) {
			LOG.error("Can't find commodity:" + e.getMessage(), e);
			addActionError("Can't find commodity!");
		}
		
		request.setCommodity(commodity);
		request.setQuantity(quantity);
		request.setName(name);
		request.setDescription(description);

		if (id!=0){
			
			request.setId(id);
			try {
				requestDao.update(request);
			} catch (SolidComDaoException e) {
				LOG.error("Can't save request:" + e.getMessage(), e);
				addActionError("Can't save request!");
			}
		} else {
			try {
				requestDao.save(request);
			} catch (SolidComDaoException e) {
				LOG.error("Can't save request:" + e.getMessage(), e);
				addActionError("Can't save request!");
			}
		}
						
		return ActionSupport.SUCCESS;
 
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Commodity> getCommodityList() {
		return commodityList;
	}

	public void setCommodityList(List<Commodity> commodityList) {
		this.commodityList = commodityList;
	}

	public long getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(long commodityId) {
		this.commodityId = commodityId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
