/**
 * 
 */
package ua.com.goit.volonteurs.solidcommunity.web.actions.response;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.ResponseDao;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;
import ua.com.goit.volonteurs.solidcommunity.domain.Response;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author SASH
 *
 */
public class ResponseRetriever extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = -3511384603193302953L;
    private static final Logger LOG = Logger.getLogger(ResponseRetriever.class);
    
    private long id;
    private Response response = null;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return the response
     */
    public Response getResponse() {
        return response;
    }
    /**
     * @param response the response to set
     */
    public void setResponse(Response response) {
        this.response = response;
    }
    
    public String execute() throws Exception {
        ResponseDao responseDao = new ResponseDao();
        try {
            response = responseDao.retrieve(id);
            return ActionSupport.SUCCESS;
        } catch (Exception e) {
            LOG.error("Can't retrieve response: " + e.getMessage(), e);
            addActionError("Can't retrive response!");
            return ActionSupport.ERROR;
        }
    }
}
