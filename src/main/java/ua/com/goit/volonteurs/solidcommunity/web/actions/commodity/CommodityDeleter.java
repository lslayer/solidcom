package ua.com.goit.volonteurs.solidcommunity.web.actions.commodity;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.CommodityDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Commodity;

import com.opensymphony.xwork2.ActionSupport;

public class CommodityDeleter extends ActionSupport {

	private static final long serialVersionUID = -8806285455060309352L;
	private static final Logger LOG = Logger.getLogger(CommodityDeleter.class);
	
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String execute() throws Exception {
		CommodityDao dao = new CommodityDao();
		Commodity commodity = null;
		try {
			commodity = dao.retrieve(Long.parseLong(id));
		} catch (NumberFormatException | SolidComDaoException e) {
			LOG.error("Can not retrieve Commodity by id: " + e.getMessage(), e);
			addActionError("Can not retrieve Commodity by id!");
			return ActionSupport.ERROR;
		}
		try {
			dao.delete(commodity);
		} catch (SolidComDaoException e) {
			LOG.error("Can not delete Commodity: " + e.getMessage(), e);
			addActionError("Can not delete Commodity!");
			return ActionSupport.ERROR;
		}
		return ActionSupport.SUCCESS;
	}
}














