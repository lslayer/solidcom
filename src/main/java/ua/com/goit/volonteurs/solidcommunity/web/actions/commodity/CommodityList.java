package ua.com.goit.volonteurs.solidcommunity.web.actions.commodity;

import java.util.List;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.CommodityDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Commodity;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;
import ua.com.goit.volonteurs.solidcommunity.services.Paginator;

import com.opensymphony.xwork2.ActionSupport;

public class CommodityList extends ActionSupport {

	
	private static final long serialVersionUID = -542906966985743467L;
	private static final Logger LOG=Logger.getLogger(CommodityList.class);
	// TODO need to change DAO-class to Service-class
	private CommodityDao сommodityDao;
	private Paginator paginator = new Paginator();
	private List<Commodity> commodities;
	private boolean delete;
	
	public String execute() throws Exception {
		
		paginator.checkAction();
		
		if(delete == true){
			try {
				delete();
				retrieve(paginator);
			} catch (SolidComDaoException e) {
				LOG.error("Can not delete Commodity: " + e.getMessage(), e);
				addActionError("Can not delete Commodity!");
				return ActionSupport.ERROR;
			}
		} else { 
			try {
				retrieve(paginator);
			} catch (SolidComDaoException e) {
				LOG.error("Can not retrieve Commodity List: " + e.getMessage(), e);
				addActionError("Can not retrieve Commodity List!");
				return ActionSupport.ERROR;
			}
		}
			
		return ActionSupport.SUCCESS;
	 }

	private void retrieve(Paginator paginator) throws SolidComDaoException {
		
		сommodityDao = ApplicationContextProvider.getApplicationContext().getBean(CommodityDao.class);
		commodities = сommodityDao.retrieveAll(paginator);
		
	}

	
	private void delete() throws SolidComDaoException {
		// TODO Auto-generated method stub
		
	}
	
	public CommodityDao getСommodityDao() {
		return сommodityDao;
	}
	public void setСommodityDao(CommodityDao сommodityDao) {
		this.сommodityDao = сommodityDao;
	}
	public List<Commodity> getCommodities() {
		return commodities;
	}
	public void setCommodities(List<Commodity> commodities) {
		this.commodities = commodities;
	}
	public boolean getDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	public Paginator getPaginator() {
		return paginator;
	}
	public void setPaginator(Paginator paginator) {
		this.paginator = paginator;
	}
	
}
