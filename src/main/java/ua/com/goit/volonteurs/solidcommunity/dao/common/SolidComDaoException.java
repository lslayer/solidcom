package ua.com.goit.volonteurs.solidcommunity.dao.common;

public class SolidComDaoException extends Exception{
	private static final long serialVersionUID = 1L;
	private String message;

	public SolidComDaoException() {
		super();
	}

	public SolidComDaoException(String arg0) {
		super(arg0);
	}

	public SolidComDaoException(String arg0, Exception e) {
		super(arg0, e);
	}

	public String getText() {
		return message;
	}
}
