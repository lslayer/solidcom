package ua.com.goit.volonteurs.solidcommunity.web.actions.request;

import java.util.List;

import org.apache.log4j.Logger;

import ua.com.goit.volonteurs.solidcommunity.dao.CommodityDao;
import ua.com.goit.volonteurs.solidcommunity.dao.RequestDao;
import ua.com.goit.volonteurs.solidcommunity.dao.common.SolidComDaoException;
import ua.com.goit.volonteurs.solidcommunity.domain.Commodity;
import ua.com.goit.volonteurs.solidcommunity.domain.Request;
import ua.com.goit.volonteurs.solidcommunity.services.ApplicationContextProvider;

import com.opensymphony.xwork2.ActionSupport;

public class RequestUpdater extends ActionSupport {

	private static final long serialVersionUID = 4430489054425784832L;
	private static final Logger LOG = Logger.getLogger(RequestUpdater.class);
	
	private long id;
	private String name;
	private int quantity;
	private String description;
	private List<Commodity> commodityList = null;
	private Commodity commodity;
	
	
	public RequestUpdater() {
		
	}
	
	public String execute(){
		
		CommodityDao commDao = ApplicationContextProvider.getApplicationContext().getBean(CommodityDao.class);
		try {
			setCommodityList(commDao.retrieveAll());
		} catch (SolidComDaoException e) {
			LOG.error("Can't find commodity:" + e.getMessage(), e);
			addActionError("Can't find commodity!");
		}
		
		RequestDao requestDao = ApplicationContextProvider.getApplicationContext().getBean(RequestDao.class);
		Request request = null;
		try {
			request = requestDao.retrieve(id);
		} catch (SolidComDaoException e) {
			LOG.error("Can't find request:" + e.getMessage(), e);
			addActionError("Can't find request!");
		}
		
		name = request.getName();
		quantity = request.getQuantity();
		description = request.getDescription();
		setCommodity(request.getCommodity());
		
		return ActionSupport.SUCCESS;
	}
	
		public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Commodity> getCommodityList() {
		return commodityList;
	}

	public void setCommodityList(List<Commodity> commodityList) {
		this.commodityList = commodityList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Commodity getCommodity() {
		return commodity;
	}

	public void setCommodity(Commodity commodity) {
		this.commodity = commodity;
	}
		
}
