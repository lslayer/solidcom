package ua.com.goit.volonteurs.solidcommunity.dao;

import ua.com.goit.volonteurs.solidcommunity.dao.common.PSQLGenericDao;
import ua.com.goit.volonteurs.solidcommunity.domain.Commodity;

public class CommodityDao extends PSQLGenericDao<Commodity>  {

	public CommodityDao() {
		super(Commodity.class);
	}
	
}
