package ua.com.goit.volonteurs.solidcommunity.domain;

import java.io.Serializable;

public class Request implements Serializable{

	private static final long serialVersionUID = -2368876509461461165L;

	private long id;
	private Commodity commodity;
	private int quantity;
	private String name;
	private String description;
	
	public Request() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Commodity getCommodity() {
		return commodity;
	}

	public void setCommodity(Commodity commodity) {
		this.commodity = commodity;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
}
